" Vim color file
" Maintainer:	David Stevenson <davidjstevenson@gmail.com>
" Last Change:	2011 Feb 2
" grey on black
" optimized for TFT panels

set background=dark
hi clear
if exists("syntax_on")
  syntax reset
endif
"colorscheme default
let g:colors_name = "david"

" hardcoded colors :
" GUI Comment : #80a0ff = Light blue

" GUI
highlight Normal     guifg=Grey80	guibg=Black
highlight Search     guifg=Black	guibg=Red	gui=bold
highlight Visual     guifg=#404040			gui=bold
highlight Cursor     guifg=Black	guibg=Green	gui=bold
highlight Special    guifg=Orange
highlight Comment    guifg=#80a0ff
highlight StatusLine guifg=blue		guibg=white
highlight Statement  guifg=Yellow			gui=NONE
highlight Type						gui=NONE

" Console
highlight Normal        ctermfg=White	ctermbg=Black
highlight Visual        cterm=reverse
highlight Comment       ctermfg=Brown
highlight Comment2      ctermfg=Blue
highlight NewSection    ctermfg=Black ctermbg=DarkGrey
highlight Number        ctermfg=DarkGreen
highlight StorageClass  ctermfg=DarkGreen
highlight Function      ctermfg=DarkCyan
highlight Delimiter     ctermfg=DarkCyan
highlight matlabDelimiter     ctermfg=Yellow
highlight matlabBrackets     ctermfg=DarkRed
highlight Special       ctermfg=Yellow
highlight matlabOperator ctermfg=Yellow
highlight Operator      ctermfg=Yellow
highlight Statement     ctermfg=Yellow
highlight Special       ctermfg=Yellow
highlight Label         ctermfg=Brown
highlight Boolean       ctermfg=Yellow
highlight Constant      ctermfg=Red
highlight String        ctermfg=Red
highlight matlabConditional ctermfg=DarkGreen
highlight matlabTODO    ctermfg=White ctermbg=Red
highlight Folded        ctermfg=Black ctermbg=Grey
"highlight Search     ctermfg=Black	ctermbg=Red	cterm=NONE
"highlight Cursor     ctermfg=Black	ctermbg=Green	cterm=bold
"highlight Special    ctermfg=Yellow
"highlight Comment    ctermfg=LightBlue
"highlight StatusLine ctermfg=blue	ctermbg=white
"highlight Statement  ctermfg=Yellow			cterm=NONE
"highlight Type	     cterm=NONE
"highlight String     ctermfg=Red
"highlight Function   ctermfg=White

" only for vim 5
if has("unix")
  if v:version<600
    highlight Normal  ctermfg=Grey	ctermbg=Black	cterm=NONE	guifg=Grey80      guibg=Black	gui=NONE
    highlight Search  ctermfg=Black	ctermbg=Red	cterm=bold	guifg=Black       guibg=Red	gui=bold
    highlight Visual  ctermfg=Black	ctermbg=yellow	cterm=bold	guifg=#404040			gui=bold
    highlight Special ctermfg=LightBlue			cterm=NONE	guifg=LightBlue			gui=NONE
    highlight Comment ctermfg=Cyan			cterm=NONE	guifg=LightBlue			gui=NONE
  endif
endif


" Some random options
set nocompatible "no Vi compatibility
set backspace=2
set number
set tw=79
set cindent
set autoindent
set foldmethod=syntax
set foldlevelstart=20
set expandtab
set shiftwidth=4
set tabstop=4
set tw=79
set colorcolumn=80
highlight ColorColumn ctermbg=233
highlight Search ctermbg=DarkBlue
highlight Folded ctermbg=233
set lbr
set wrap
set formatoptions=1
" Smart backspace
set backspace=start,indent,eol
set hidden "dont warn about leaving modified buffer
set showcmd "show command as i type
set laststatus=2   " Always show the statusline
set encoding=utf-8 " Necessary to show Unicode glyphs
set t_Co=256 " Explicitly tell Vim that the terminal supports 256 colors
set clipboard=unnamed
set pastetoggle=<F2>
set mouse=a
syntax on

" show whitespace
match ErrorMsg '\s\+$'

" source the vimrc every time we save it
autocmd! bufwritepost .vimrc source %

" this prevent the # being moved to the start of the line
" have comment out because the problem was caused by smartindent
" leaving here for reference
"inoremap # X#

filetype off
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

set rtp+=~/.vim/bundle/powerline/powerline/bindings/vim

Bundle 'gmarik/vundle'
Bundle 'bufexplorer.zip'
Bundle 'tpope/vim-surround'
Bundle 'scrooloose/nerdcommenter'
Bundle 'mattn/zencoding-vim'
Bundle 'vim-scripts/matchit.zip'
Bundle 'vim-scripts/L9.git'
Bundle 'tpope/vim-fugitive'
Bundle 'tpope/vim-repeat'
Bundle 'tpope/vim-endwise'
Bundle 'FuzzyFinder'
"Bundle 'Rip-Rip/clang_complete'
Bundle 'majutsushi/tagbar'
Bundle 'yandy/vim-omnicppcomplete'
Bundle 'a.vim'
Bundle 'scrooloose/syntastic'
Bundle 'Lokaltog/powerline'
"Bundle 'autoproto.vim'
Bundle 'garbas/vim-snipmate'
Bundle 'ervandew/supertab'
Bundle 'MarcWeber/vim-addon-mw-utils'
Bundle 'kien/ctrlp.vim'
"Bundle 'xolox/vim-misc'
"Bundle 'xolox/vim-easytags'

filetype plugin indent on

let g:Powerline_symbols = 'fancy'

source ~/.vim/cscope_maps.vim

" ============
" OMNICOMPLETE
" ============
" configure tags - add additional tags here or comment out not-used ones
set tags+=~/.vim/tags/cpp
set tags+=~/.vim/tags/qt4
" build tags of your own project with Ctrl-F12
map <C-F12> :!ctags -R --sort=yes --c++-kinds=+pl --fields=+iaS --extra=+qf .<CR>

" OmniCppComplete
let OmniCpp_NamespaceSearch = 1
let OmniCpp_GlobalScopeSearch = 1
let OmniCpp_ShowAccess = 1
let OmniCpp_ShowPrototypeInAbbr = 1 " show function parameters
let OmniCpp_MayCompleteDot = 1 " autocomplete after .
let OmniCpp_MayCompleteArrow = 1 " autocomplete after ->
let OmniCpp_MayCompleteScope = 1 " autocomplete after ::
let OmniCpp_DefaultNamespaces = ["std", "_GLIBCXX_STD"]
" automatically open and close the popup menu / preview window
au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif
set completeopt=menuone,menu,longest,preview
autocmd FileType cpp set omnifunc=omni#cpp#complete#Main

" Much improved auto completion menus
set completeopt=menuone,longest,preview

" ============
" C++ STUFF
" ============
" Add highlighting for function definition in C++
function! EnhanceCppSyntax()
  syn match cppFuncDef "::\~\?\zs\h\w*\ze([^)]*\()\s*\(const\)\?\)\?$"
  hi def link cppFuncDef Special
endfunction

autocmd Syntax cpp call EnhanceCppSyntax()


" ========
" MAPPINGS
" ========

let mapleader = ","

vmap Q gq
nmap Q gqap

"tab switching
map <Leader>n <ESC>:tabprevious<CR>
map <Leader>m <ESC>:tabnext<CR>

"split navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

"sort
vnoremap <Leader>s :sort<CR>

"indentation
vnoremap < <gv
vnoremap > >gv

noremap <F3> :TagbarToggle<CR>

" Syntastic
noremap <silent> <F4> :SyntasticCheck<CR>
let g:syntastic_check_on_open=1
let g:syntastic_always_populate_loc_list=1
let g:syntastic_enable_highlighting = 0

"noremap! <silent> <F4> <ESC>:SyntasticCheck<CR>
"let g:syntastic_mode_map = {
"    \ 'mode': 'active',
"    \ 'active_filetypes': [],
"    \ 'passive_filetypes': ['python', 'cpp', 'c'] }

" incremental search that highlights results
set incsearch
set hlsearch
"" Ctrl-L clears the highlight from the last search
noremap <C-m> :nohlsearch<CR>

"
"" TAB and Shift-TAB in normal mode cycle buffers
"
noremap <Tab> :bn<CR>
noremap <S-Tab> :bp<CR>


" map that tab key to esc
"nnoremap <Tab> <Esc>
"vnoremap <Tab> <Esc>gV
"onoremap <Tab> <Esc>
"inoremap <Tab> <Esc>`^
"inoremap <Leader><Tab> <Tab>
"au VimEnter * map <Tab> <Esc>
"au VimEnter * imap <Tab> <Esc>
"au VimEnter * vmap <Tab> <Esc>

" spelling options
let spfile=expand("%:p:h")."/en.utf-1.add"
if  filereadable(spfile)
    exec "map <F7> :setlocal spell spelllang=en_nz spellfile=~/.vim/spell/en.utf-8.add,".escape(spfile,' ')."<CR>"
else
    map <F7> :setlocal spell spelllang=en_nz spellfile=~/.vim/spell/en.utf-8.add<CR>
endif 
map <F6> :setlocal nospell<CR>

" reload .vimrc
map <silent> <C-n> :source $MYVIMRC<CR>

" call Sum() on current selection
vmap <M-s> y:call Sum()<CR>

" save then, make project, second <CR> get you back to vim from std
"nmap <F8> :w<CR> :!make 2>/dev/null 1>pipe &<CR><CR>
"nmap <F8> :w<CR> :!make <CR><CR>
"imap <F8> <ESC>:w<CR> :!make <CR><CR>i

" move by 'display lines' instead of 'logical lines'
nmap <silent> j gj
nmap <silent> k gk

" ========================
" PERSONAL IN-VIMRC SCRIPS
" ========================

" ADD SOME RULES FOR MATLAB FILES
" *******************************
autocmd FileType matlab syn match matlabStorageClass		"properties "
autocmd FileType matlab syn match matlabBrackets		"[][]"
autocmd FileType matlab syn match matlabBrackets		"[][()]"
autocmd FileType matlab syn match matlabBrackets		"[][{}]"
autocmd FileType matlab syn match matlabDelimiter		"[][,:@]"
"autocmd FileType matlab syn match matlabDelimiter		"[\a)]\@<=\."
"autocmd FileType matlab syn match matlabDelimiter		"\.\@>=[(]"
autocmd FileType matlab syn match matlabDelimiter		"\d\@!\."
autocmd FileType matlab syn match matlabNewSection		"%%.*$"	contains=matlabTodo,matlabTab
autocmd FileType matlab syn match matlabComment2		"%|.*$"	contains=matlabTodo,matlabTab
autocmd FileType matlab syn match matlabComment			"%[^%|].*$"	contains=matlabTodo,matlabTab
autocmd FileType matlab syn match matlabComment			"%$"	contains=matlabTodo,matlabTab
autocmd FileType matlab syn match matlabFunc			"@\@<=\w\+"
autocmd FileType matlab syn match matlabFunc			"\(\.\|\w\)\+\(( \)\@="

" interacting with screen
" ***********************

" sending function; user does not call directly
" - window can be ""
" - session can be ""
" - %p = full path
" - %pr = root of file name, no extension
" - %r = the relative path, no extension
"
" a: means the following variable is a function argument
" b: means the following variable is local to the buffer
"
function! SendToScreenWindow(session, window, text)
	let pArg = a:window == "" ? "" : "-p " . a:window
	let sArg = a:session == "" ? "" : "-S " . a:session
    " replace the path tokens
	let toStuff = substitute(a:text,  "%p", fnamemodify(@%, ":p"), "g")
	let toStuff = substitute(toStuff, "%pr", fnamemodify(@%, ":p:r"), "g")
	let toStuff = substitute(toStuff, "%r", fnamemodify(@%, ":r"), "g")
    " escape all single quotes
    let toStuff = substitute(toStuff, "'", "'\\\\''", 'g')

    " send the text away to the appropriate screen
	call system("screen " . sArg . " " . pArg . " -X stuff '" . toStuff . "\n'")
endfunction
" commands to set run-time options
command! -nargs=+ SetRunTimeOptions   let b:runTimeOpts=<q-args>
command!          ClearRunTimeOptions let b:runTimeOpts=""

" command to send text to active window
command! -nargs=+ SendToScreen call SendToScreenWindow("", <q-args>)


" FILETYPE CONFIURATIONS
" **********************
function! ConfigurePython()
	nmap <silent> R :wa<CR> :call SendToScreenWindow("python", "python", "execfile('%r.py') " . b:runTimeOpts)<CR>
	vmap <silent> R y:silent call SendToScreenWindow("python", "python", @")<CR>
endfunction

function! ConfigureMatlab()
    nmap M :call SendCustom("ML","matlab")<CR>
	nmap <silent> R :wa<CR>:call SendToScreenWindow("ML", "matlab", "%r " . b:runTimeOpts)<CR>
	vmap <silent> R y:silent call SendToScreenWindow("ML", "matlab", @")<CR>
    nmap <silent> <C-j> :call SendToScreenWindow("ML","matlab","dbquit")<CR>
    nmap <silent> <C-n> :call SendToScreenWindow("ML","matlab","dbstep")<CR>
    nmap <silent> <C-N> :call SendToScreenWindow("ML","matlab","dbcont")<CR>
    nmap <silent> <C-h> :call SendToScreenWindow("ML","matlab","ws")<CR>
    nmap <silent> <C-m> :w<CR> :call DefaultScript()<CR>
    nmap <silent> S :call SendToScreenWindow("ML","matlab","<C-c>")<CR>
endfunction

function! SendCustom(sessionName, tabName)
    call inputsave()
    let com = input(a:tabName . ' command: ')
    call inputrestore()
    call SendToScreenWindow(a:sessionName,a:tabName,com)
endfunction

function! DefaultScript()
    if exists("g:DS")
        call SendToScreenWindow("matlab",g:DS)
    else
        echo "no script set, execute :let g:DS=\"default script name\""
    endif
endfunction

" SUM COLUMN OF NUMBERS IN YANKED REGISTER, SAVE
" **********************************************
" result to " register
function! Sum()
python << EOF
import vim

# find total from input
items = vim.eval("@0").split("\n")
floatsList = map(float, filter(lambda n: n != '', items))
total = sum(floatsList)

# set register and echo result
vim.command("call setreg('\"', '%s\n')" % total)
vim.command("echo '%s saved to clipboard'" % total)
EOF
endfunction

function! s:DiffWithSaved()
  let filetype=&ft
  diffthis
  vnew | r # | normal! 1Gdd
  diffthis
  exe "setlocal bt=nofile bh=wipe nobl noswf ro ft=" . filetype
endfunction
com! DiffSaved call s:DiffWithSaved()
